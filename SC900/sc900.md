# SC-900: Microsoft Security, Compliance, and Identity Fundamentals

# Contents

1. [Describe the Concepts of Security, Compliance, and Identity (5-10%)](#secconcepts)
2. [Describe the capabilities of Microsoft Identity and Access Management Solutions (25-30%)](#capabilities)
3. [Describe the capabilities of Microsoft Security Solutions (30-35%)](#msscapabilities)
4. [Describe the Capabilities of Microsoft Compliance Solutions (25-30%) <a name=""></a>](#mcscapabilities)
5. [Resources](#resources)

****

## Describe the Concepts of Security, Compliance, and Identity (5-10%) <a name="secconcepts"></a>
### Describe security methodologies
#### Describe the Zero-Trust methodology

****

#### Describe the shared responsibility model

****

#### Define defense in depth

****

### Describe security concepts
#### Describe common threats

****

#### Describe encryption

****

### Describe Microsoft Security and compliance principles
#### Describe Microsoft's privacy principles

****

#### Describe the offerings of the service trust portal

****

## Describe the capabilities of Microsoft Identity and Access Management Solutions (25-30%) <a name="capabilities"></a>
### Define identity principles/concepts
#### Define identity as the primary security perimeter

****

#### Define authentication

****

#### Define authorization

****

#### Describe what identity providers are

****

#### Describe what Active Directory is

****

#### Describe the concept of Federated services

****

#### Define common Identity Attacks

****


### Describe the basic identity services and identity types of Azure AD
#### Describe what Azure Active Directory is

****

#### Describe Azure AD identities (users, devices, groups, service principals/applications)

****

#### Describe what hybrid identity is

****

#### Describe the different external identity types (Guest Users)

****


### Describe the authentication capabilities of Azure AD
#### Describe the different authentication methods

****

#### Describe self-service password reset

****

#### Describe password protection and management capabilities

****

#### Describe Multi-factor Authentication

****

#### Describe Windows Hello for Business

****

### Describe access management capabilities of Azure AD
#### Describe what conditional access is

****

#### Describe uses and benefits of conditional access

****

#### Describe the benefits of Azure AD roles

****


### Describe the identity protection & governance capabilities of Azure AD
#### Describe what identity governance is

****

#### Describe what entitlement management and access reviews is

****

#### Describe the capabilities of PIM

****

#### Describe Azure AD Identity Protection

*****

## Describe the capabilities of Microsoft Security Solutions (30-35%) <a name="msscapabilities"></a>
### Describe basic security capabilities in Azure
#### Describe Azure Network Security groups

****

#### Describe Azure DDoS protection

****

#### Describe what Azure Firewall is

****

#### Describe what Azure Bastion is

****

#### Describe what Web Application Firewall is

****

#### Describe ways Azure encrypts data

****


### Describe security management capabilities of Azure
#### Describe the Azure Security center

****

#### Describe Azure Secure score

****

#### Describe the benefit and use cases of Azure Defender - previously the cloud workload protection platform (CWPP)

****

#### Describe Cloud security posture management (CSPM)

****

#### Describe security baselines for Azure

****


### Describe security capabilities of Azure Sentinel
#### Define the concepts of SIEM, SOAR, XDR

****

#### Describe the role and value of Azure Sentinel to provide integrated threat protection

****

### Describe threat protection with Microsoft 365 Defender (formerly Microsoft Threat Protection)
#### Describe Microsoft 365 Defender services

****

#### Describe Microsoft Defender for Identity (formerly Azure ATP)

****

#### Describe Microsoft Defender for Office 365 (formerly Office 365 ATP)

****

#### Describe Microsoft Defender for Endpoint (formerly Microsoft Defender ATP)

****

#### Describe Microsoft Cloud App Security

****


### Describe security management capabilities of Microsoft 365
#### Describe the Microsoft 365 Security Center

****

#### Describe how to use Microsoft Secure Score

****

#### Describe security reports and dashboards

****

#### Describe incidents and incident management capabilities

****


### Describe endpoint security with Microsoft Intune
#### Describe what Intune is

****

#### Describe endpoint security with Intune

****

#### Describe the endpoint security with the Microsoft Endpoint Manager admin center

****

## Describe the Capabilities of Microsoft Compliance Solutions (25-30%) <a name="mcscapabilities"></a>
### Describe the compliance management capabilities in Microsoft
#### Describe the compliance center

****

#### Describe compliance manager

****

#### Describe use and benefits of compliance score

****


### Describe information protection and governance capabilities of Microsoft 365
#### Describe data classification capabilities

****

#### Describe the value of content and activity explorer

****

#### Describe sensitivity labels

****

#### Describe Retention Polices and Retention Labels

****

#### Describe Records Management

****

#### Describe Data Loss Prevention

****


### Describe insider risk capabilities in Microsoft 365
#### Describe Insider risk management solution

****

#### Describe communication compliance

****

#### Describe information barriers

****

#### Describe privileged access management

****

#### Describe customer lockbox

****


### Describe the eDiscovery capabilities of Microsoft 365
#### Describe the purpose of eDiscovery

****

#### Describe the capabilities of the content search tool

****

#### Describe the core eDiscovery workflow

****

#### Describe the advanced eDisovery workflow

****


### Describe the audit capabilities in Microsoft 365
#### Describe the core audit capabilities of M365

****

#### Describe purpose and value of Advanced Auditing

****

### Describe resource governance capabilities in Azure
#### Describe the use of Azure Resource locks

****

#### Describe what Azure Blueprints is

****

#### Define Azure Policy and describe its use cases

****

#### Describe cloud adoption framework

****

## Resources <a name="resources"></a>
1. [Microsoft Certified: Security, Compliance, and Identity Fundamentals](https://docs.microsoft.com/en-us/learn/certifications/security-compliance-and-identity-fundamentals/)
2. [Microsoft Certified: Security, Compliance, and Identity Fundamentals – Skills Measured](https://query.prod.cms.rt.microsoft.com/cms/api/am/binary/RE4Myp7)
3. [SC-900 part 1: Describe the concepts of security, compliance, and identity](https://docs.microsoft.com/en-us/learn/paths/describe-concepts-of-security-compliance-identity/)
4. [SC-900 part 2: Describe the capabilities of Microsoft Identity and access management solutions](https://docs.microsoft.com/en-us/learn/paths/describe-capabilities-of-microsoft-identity-access/)
5. [SC-900 part 3: Describe the capabilities of Microsoft security solutions](https://docs.microsoft.com/en-us/learn/paths/describe-capabilities-of-microsoft-security-solutions/)
6. [SC-900 part 4: Describe the capabilities of Microsoft compliance solutions](https://docs.microsoft.com/en-us/learn/paths/describe-capabilities-of-microsoft-compliance-solutions/)
7. [John Savill:SC-900 Microsoft Security, Compliance, and Identity Fundamentals Study Cram](https://www.youtube.com/watch?v=Bz-8jM3jg-8)
