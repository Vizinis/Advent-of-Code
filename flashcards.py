import xlrd     # Use sheets with python
import os       # for clearing terminal
import random   # generating sort of random numbers

# Open sheet with cards
f = xlrd.open_workbook("../../Notes/Splunk Notes/LearningCards.xlsx")
sheet = f.sheet_by_index(0)


rows = sheet.nrows                      # length of the sheet
index = random.randint(0, rows - 1)     # generates a random number in range of length of the document
while True:
    os.system('cls' if os.name == 'nt' else 'clear')    # clears terminal for every question
    print(f'{sheet.cell_value(index, 0)} \n \n')              # question
    pause = input("")                                   # pause until any button is entered, enter for EPIC SPEED
    print(f'{sheet.cell_value(index, 1)}')              # answer
    pause = input("")                                   # pause to check answer
    index = random.randint(0, rows - 1)                 # generates index for new question

    # needs improvement, better way to end the program
    if pause == "q":
        break
