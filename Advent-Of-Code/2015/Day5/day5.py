# https://stackoverflow.com/questions/14012562/how-to-compare-two-adjacent-items-in-the-same-list-python
# zip()
# any()
# joining pairs
# https://stackoverflow.com/questions/5850986/joining-pairs-of-elements-of-a-list


def double_letters(puzzle):     # check if there are double letter pairs in the given string
    for x, y in zip(puzzle, puzzle[1:]):
        if x == y:
            return True
    return False


def vowel_check(puzzle):
    vowels = ['a', 'e', 'i', 'o', 'u']
    count = 0               # check if the vowels appear in given strings
    for i in puzzle:        # and count how many times.
        if i in vowels:     # if at least 3 return true
            count += 1

    if count >= 3:
        return True
    else:
        return False


def naughty_string(puzzle):
    naughties = ['ab', 'cd', 'pq', 'xy']        # banned word list

    if any(i in puzzle for i in naughties):     # check if banned words appear in given strings
        return False
    else:
        return True


def solution1(puzzle):
    nice = 0
    for i in puzzle:        # go trough the list and check for requirements
        if double_letters(i) and vowel_check(i) and naughty_string(i):
            nice += 1
    print(f'Part 1 nice words: {nice}')


def double_letters2(puzzle):
    for x, y in zip(puzzle, puzzle[2:]):    # skip one letter and check the next one.
        if x == y:
            return True
    return False


# print([i + j for i, j in zip(puzzle[::1], puzzle[1::1])])
def not_overlapping(puzzle):    # extract list of pairs from given string
    temp_pairs = ([i + j for i, j in zip(puzzle[::1], puzzle[1::1])])

    for i in temp_pairs:    # for every pair check how many times it appears in the string
        count = 0           # reset count for every pair
        temp = puzzle       # reset string before while loop, where it gets edited.
        while i in temp:
            temp = temp.replace(i, " ", 1)  # remove pair from string, replace it with a space or a random char
            count += 1                      # count amount of times the pair appears in the string

            if count >= 2:                  # check if it fits the nice word list
                return True
    return False


def solution2(puzzle):
    count = 0
    for i in puzzle:            # go trough the list and check for requirements
        if double_letters2(i) and not_overlapping(i):
            count += 1
    print(f'Part 2 nice words: {count}')


f = open("input.txt", "r")
puzzle = [] # input into variable, close file after

for i in f.readlines():
    puzzle.append(format(i.strip()))
f.close()

test1 = ['ugknbfddgicrmopn', 'aaa', 'jchzalrnumimnmhp', 'haegwjzuvuyypxyu', 'dvszwmarrgswjxmb']
test2 = ['qjhvhtzxzqqjkmpb', 'xxyxx', 'uurcxstgmygtbstg', 'ieodomkazucvgmuy']

solution1(puzzle)
solution2(puzzle)

