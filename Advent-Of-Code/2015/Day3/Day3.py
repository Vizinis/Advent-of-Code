def visited_houses(f):
    coordinates = []                # houses visited coordinate list
    x, y, unique_visited = 0, 0, 0  # coordinates, house count

    for i in f:
        if i == "^":
            y += 1
        elif i == "v":
            y -= 1
        elif i == ">":
            x += 1
        elif i == "<":
            x -= 1
        # check if already visited, if not visit and increase count of visited houses (present)
        if (x, y) not in coordinates:
            coordinates.append((x, y))
            unique_visited += 1

    print(f'Solution #1: {unique_visited} houses visited.')


def visited_houses2(f):
    santa, irobot = [], []
    xrobot, yrobot = 0, 0
    xsanta, ysanta = 0, 0

    index = 0
    for i in f:

        if index % 2 == 0:
            if i == "^":
                yrobot += 1
            elif i == "v":
                yrobot -= 1
            elif i == ">":
                xrobot += 1
            elif i == "<":
                xrobot -= 1
            # coordinates for robot, check if any of them visited already, if not add to robot list of unique houses
            if ((xrobot, yrobot) not in irobot) and ((xrobot, yrobot) not in santa):
                irobot.append((xrobot, yrobot))

        else:
            if i == "^":
                ysanta += 1
            elif i == "v":
                ysanta -= 1
            elif i == ">":
                xsanta += 1
            elif i == "<":
                xsanta -= 1
            # coordinates for robot, check if any of them visited already, if not add to santa list of unique houses
            if ((xsanta, ysanta) not in santa) and ((xsanta, ysanta) not in irobot):
                santa.append((xsanta, ysanta))
        index += 1

    print(f'Solution #2: {len(santa)+len(irobot)}')


f = open("input.txt", "r")
text = ""               # input from input.txt
for i in f.read():
    text += i
f.close()

visited_houses(text)
visited_houses2(text)