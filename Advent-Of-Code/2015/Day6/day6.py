import re


# read the file, close it and return the contents.
def read_file(file_name):
    f = open(f"{file_name}", "r")
    temp = []
    for i in f.readlines():
        temp.append(format(i.strip()))
    f.close()
    return temp


# Parse the instructions for the puzzle.
# Return the coordinates and the action in a list
# ins = [ start_x, start_y, end_x, end_y, action]
def read_instructions(puzzle):
    ins = []
    for i in puzzle:
        res = []
#        print(i)
        temp = re.findall(r'\d+', i)
        res = list(map(int, temp))

        if "on" in i:
            res.append("on")
        elif "off" in i:
            res.append("off")
        elif "toggle" in i:
            res.append("toggle")

        ins.append(res)
    # Instructions = [ start_x, start_y, end_x, end_y, action]
    return ins


# takes grid, start coords and end coords
# and then turns them false -> true or true -> false depending on current value
def toggle(grid, start, end):
    for x in range(start[0], end[0] + 1):
        for y in range(start[1], end[1] + 1):
            grid[(x, y)] = not grid.get((x, y), False)

    return grid


# takes grid, start coords and end coords and then turns lights on
def turn_on(grid, start, end):
    for x in range(start[0], end[0] + 1):
        for y in range(start[1], end[1] + 1):
            grid[(x, y)] = grid.get((x, y), True)
    return grid


# same as turn_on, just off.
def turn_off(grid, start, end):
    for x in range(start[0], end[0] + 1):
        for y in range(start[1], end[1] + 1):
            grid[(x, y)] = grid.get((x, y), False)
    return grid


# f = open("input.txt", "r")
puzzle = read_file("input.txt")
test_input = ["turn on 0,0 through 999,999", "toggle 0,0 through 999,0", "turn off 499,499 through 500,500"]

# Instructions = [ start_x, start_y, end_x, end_y, action]
instructions = read_instructions(puzzle)

a = {}      # grid of lights
index = 0   # index for loop count
for i in instructions:
    if i[4] == "on":        # if the action in instructions is on
        turn_on(a, (instructions[index][0], instructions[index][1]), (instructions[index][2], instructions[index][3]))
    elif i[4] == "off":     # if the action in instructions is off
        turn_off(a, (instructions[index][0], instructions[index][1]), (instructions[index][2], instructions[index][3]))
    elif i[4] == "toggle":  # if the action in instructions is toggle
        toggle(a, (instructions[index][0], instructions[index][1]), (instructions[index][2], instructions[index][3]))
    index += 1

                    # +1 for every value in grid if it's true
num_true = sum(1 for condition in a.values() if condition)
print(num_true)