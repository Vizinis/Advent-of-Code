# https://docs.python.org/3/library/hashlib.html
import hashlib


# part = amount of 0s required (5 for part1, 6 for part2)
def solution(puzzle, part):
    leading = 0
#    print((hashlib.md5((puzzle+str(leading)).encode('utf-8'))).hexdigest()[0:5])
    while not (hashlib.md5((puzzle+str(leading)).encode('utf-8'))).hexdigest().startswith(part *'0'):
        leading += 1
    print(f"Answer with {part} 0's: {leading}")


puzzle = "iwrupvqb"
solution(puzzle, 5)
solution(puzzle, 6)