import re   # import regex


# using regex to extract numbers from string
def extract_numbers(line):
    numbers = re.findall(r'[\d]+', line)
#    print(numbers) # check if the correct numbers are extracted
    return numbers


#   TODO
def calculate_ribbon(numbers):
    length = int(numbers[0])    # formatting strings to int
    width = int(numbers[1])
    height = int(numbers[2])

    measurements = sorted([length, width, height])
#    print((measurements[0] + measurements[1]))
#    print(length*width*height)
    return (2*(measurements[0] + measurements[1]))+(length*width*height)


# calculating the required amount of wrapping paper and the areal of the box.
# checking which ones is lowest and returning the amount of wrapping paper needed
# for the box
def calculate_areal(numbers):
    length = int(numbers[0])    # formatting strings to int
    width = int(numbers[1])
    height = int(numbers[2])

    # total_areal is a sum list of opposite areal of the box. F.eks top and bottom, side to side, front and back.
    total_areal = [2*length*width, 2*width*height, 2*height*length]

    # sides is a list of areal on one side of the box (top, side, front). To find out the smallest
    # one and add it to the sum to get the correct amount of wrapping paper.
    sides = [length*width, width*height, height*length]
    return sum(total_areal)+min(sides)

#    print(f'l: {length}, w: {width}, h: {height}')


# open day2.txt file with the inputs.
file = open("day2.txt", "r")

wrapping_paper = 0   # total amount of wrapping paper.
ribbon = 0           # ribbon
for i in file.readlines():          # read every line of the input file
    line = format(i.strip())        # .strip removes enter/spaces from lines
    nums = extract_numbers(line)  # send the strings to be split into numbers(as text) (removes 'x')

    wrapping_paper += calculate_areal(nums)  # converts strings into numbers, counts total areal and finds smallest side
#                                            returns amount of wrapping paper required to wrap a gift.
    ribbon += calculate_ribbon(nums)

#    print(format(i.strip()))    #test if format removes \n(enter spaces)

print(wrapping_paper)
print(ribbon)
file.close()