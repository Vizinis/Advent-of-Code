def solution1():
    f = open("day1.txt", "r")
    floor = 0
    for i in f.read():
        if i == '(':
            floor += 1
        else:
            floor -= 1
    print(floor)
    f.close()


def solution2():
    f = open("dayy1.txt", "r")
    count = 0
    floor = 0
    for i in f.read():
        count += 1
        if i == '(':
            floor += 1
        else:
            floor -= 1
        if floor < 0:
            print(count)
            break
    f.close()


solution1()
solution2()

