def solution1(puzzle, right, down):
    x, lines, trees = 0, 0, 0
    for i in puzzle:                            # i = lines in input
        if not (lines % down):                  # traverse "down" lines at a time
            if i[x % len(i)] == "#":            # check if there's a tree "#" or no
                trees += 1                      # increase tree count
            x += right                          # move to the right by "right"
        lines += 1                              # new line
    return trees


def solution2(puzzle):
    instr = [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]]    # [right, down] inputs given in the problem
    trees = 0
    for i in instr:
        if trees == 0:
            trees += solution1(puzzle, i[0], i[1])      # if this is the first time give trees a value
        else:
            trees *= solution1(puzzle, i[0], i[1])      # else multiply trees
#        print(f'{i}: {solution1(puzzle, i[0], i[1])} Trees so far: {trees}')
    return trees


with open("input.txt") as f:
    puzzle = [line.rstrip() for line in f]

print(solution1(puzzle, 3, 1))
print(solution2(puzzle))