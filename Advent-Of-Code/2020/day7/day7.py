import re

bags = []
bags_with_shiny = []


def bag_list(data):
    for i in data:
        contents = []
        temp = re.findall('^(\w+ \w+)', i)
        contents.append(temp[0])
        i = i.replace(temp[0] + " bags contain ", "")
        if re.findall('(\d+ \w+ \w+)', i):
            for j in (re.findall('(\d+ \w+ \w+)', i)):
                contents.append(j)
        elif "other bags" in i:
            contents.append('0')
        bags.append(contents)
    return bags


def solution1(bag_name):
    for i in bags:                                      # go trough bag list and find every bag
        for j in i[1:]:                                 # which has anything to do with a
            if bag_name in j:                           # shiny gold bag, or any bag which we give into the function
                bags_with_shiny.append(i[0])
                solution1(i[0])                         # return the num of unique bags
    return len(set(bags_with_shiny))


with open("input.txt") as f:
    data = [line.rstrip() for line in f]


def solution2(color):
    condition = ""                   # condition parameter
    for line in data:
        if line[:line.index(' bags')] == color:
            condition = line

    if 'no' in condition:
        return 1
                                                                    # remove the name of the bag and
    condition = condition[condition.index('contain') + 8:].split()  # find which bags are inside a given bag
    total = 0
    i = 0
    while i < len(condition):
        count = int(condition[i])
        color = condition[i + 1] + " " + condition[i + 2]
        total += count * solution2(color)
        i += 4
    return total + 1


bag_list(data)

print(f'Solution 1: {solution1("shiny gold")}')
print(f'Solution 2: {solution2("shiny gold")-1}')   # -1, original shiny gold bag
