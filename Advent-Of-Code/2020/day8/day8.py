def solution1(visited):
    accumulator = 0
    index = 0

    for i in range(len(visited)):
        visited[i][2] = 0

    while True:
        if visited[index][2] == 1:  # if this command already been used
            break  # stop the loop and print the answer

        if visited[index][0] == 'nop':  # nothing changes if command is 'nop'
            visited[index][2] = 1  # mark as visited
            index += 1  # increase index to access new command
        elif visited[index][0] == 'acc':  # if 'acc' increase accumulator variable by given #
            accumulator += visited[index][1]
            visited[index][2] = 1
            index += 1
        elif visited[index][0] == 'jmp':  # if 'jmp' we jump to the next command which is
            visited[index][2] = 1  # found by adding the # in the command to the search index
            index += visited[index][1]
        if index >= len(visited):  # if index is longer than the list, reached the end
            return accumulator, True  # return accumulator for part 2 of the problem. Return True to show
            # end of array
    return accumulator, False  # solution 1 answer. False is for part 2 of the problem.


# find which input can be switched to reach the end of input data
# by switching up two inputs.
def solution2(visited):
    for index in range(len(visited)):
        if 'jmp' in visited[index][0]:
            visited[index][0] = visited[index][0].replace('jmp', 'nop')  # swap commands
            accumulator, found = solution1(visited)  # run trough the inputs

            if found:  # if end is found, return accumulator value
                return accumulator

            else:  # if end is not found, return original commands, and continue
                visited[index][0] = visited[index][0].replace('nop', 'jmp')  # the search


with open("input.txt") as f:
    puzzle = [line.rstrip() for line in f]

data = []
for i in puzzle:
    data.append([i[:3], int(i[4:]), 0])

print(solution1(data)[0])
print(solution2(data))