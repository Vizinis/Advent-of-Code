import re
import time

class Passports:
    def __init__(self, data, byr=0, iyr=0, eyr=0, hgt=0, hcl=0, ecl=0, pid="", cid=0, hgt_type=0):
        self.data = data
        self.byr = byr
        self.iyr = iyr
        self.eyr = eyr
        self.hgt = hgt
        self.hcl = hcl
        self.ecl = ecl
        self.pid = pid
        self.cid = cid
        self.hgt_type = hgt_type  # height type

        self.gib_values(data)

    def gib_values(self, data):
        for i in data:
            if "byr:" in i:                     # (Birth Year) - four digits; at least 1920 and at most 2002.
                i = int(i.replace("byr:", ""))  # Remove tag, change to int
                if 1920 <= i <= 2002:           # check rule, assign
                    self.byr = i
            elif "iyr:" in i:                   # (Issue Year) - four digits; at least 2010 and at most 2020.
                i = int(i.replace("iyr:", ""))  # Remove tag, assign int
                if 2010 <= i <= 2020:           # Check rule, assign
                    self.iyr = i
            elif "eyr:" in i:                   # (Expiration Year) - four digits; at least 2020 and at most 2030.
                i = int(i.replace("eyr:", ""))  # Remove tag, assign int
                if 2020 <= i <= 2030:           # Check rule, assign
                    self.eyr = i
            elif "hgt:" in i:                   # (Height) - a number followed by either cm or in:
                i = i.replace("hgt:", "")       # Remove tag
                if "in" in i:                   # If in, the number must be at least 59 and at most 76.
                    i = i.replace("in", "")     # remove inches tag
                    self.hgt_type = "in"        # add height type
                    if 59 <= int(i) <= 76:      # check rule
                        self.hgt = int(i)       # change to int, assign
                elif "cm" in i:                 # If cm, the number must be at least 150 and at most 193.
                    i = i.replace("cm", "")     # remove cm tag
                    self.hgt_type = "cm"        # add height type
                    if 150 <= int(i) <= 193:    # check rule, change to int, assign
                        self.hgt = int(i)
            elif "hcl:" in i:                   # (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
                i = i.replace("hcl:", "")                           # remove tag
                if re.findall('^#(?:[0-9a-fA-F]{3}){1,2}$', i):     # find color code which fits the rule, assign
                    self.hcl = i
            elif "ecl:" in i:
                i = i.replace("ecl:", "")       # (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
                if i == "amb" or i == "blu" or i == "brn" or i == "gry" or i == "grn" or i == "hzl" or i == "oth":
                    self.ecl = i
            elif "pid:" in i:                   # (Passport ID) - a nine-digit number, including leading zeroes.
                i = i.replace("pid:", "")       # remove tag
                if re.findall('^[0-9]{9}', i):  # find ID's that fit the rule
                    self.pid = i
            elif "cid:" in i:                   # assign cid, useless, but still.
                self.cid = i.replace("cid:", "")


def parse_lines(puzzle):                    # for solution 1
    passport_list = []                      # make a list of passport information
    string = ""

    for i in puzzle:                        # parse every item in puzzle to construct passport string
        if i == "":                         # if \n add passport string to the list
            passport_list.append(string)
            string = ""                     # reset string
        else:                               # if line doesn't end, add line to the string
            string += " " + i
    passport_list.append(string)            # add the last string which doesn't have empty string following it
    return passport_list


def parse_input(puzzle):
    passport = []                           # passport information in separate strings name:var
    passport_list = []                      # list of passwords
    string = ""
    for i in puzzle:
        if i == "":                         # if line ends
            passport_list.append(passport)  # add passport to the list
            passport = []                   # make a clean passport
        else:
            for j in i:
                if j == ":":                # if we find : continue building string
                    string += ":"
                elif j == " ":              # if we find ' ' add passport information to the passport
                    passport.append(string)
                    string = ""             # reset string
                else:
                    string += j             # build passport information
            passport.append(string)         # finish constructing last password detail
            string = ""                     # reset string
    passport_list.append(passport)          # add passport to the list
    return passport_list


def solution1(passport_list):
    req = ["byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:"]  # requirement passport fields

    valid_count = 0
    for i in passport_list:
        if (req[0] in i)\
                and (req[1] in i)\
                and (req[2] in i)\
                and (req[3] in i)\
                and (req[4] in i)\
                and (req[5] in i)\
                and (req[6] in i):
            valid_count += 1     # check if every passport contains requirements and count them
    return valid_count


def solution2(data):
    valid = 0
    for i in data:    # parsing all passports, checking valid passports and counting them.
        if i.byr > 0\
                and i.iyr > 0\
                and i.eyr > 0\
                and i.hgt > 0\
                and i.hcl\
                and i.ecl\
                and (len(i.pid) == 9):
            valid += 1
    return valid

start_time = time.time()
with open("input.txt") as f:
    puzzle = [line.rstrip() for line in f]


print(f'Solution 1: {solution1(parse_lines(puzzle))} [Runtime: {round(((time.time() - start_time)*1000), 3)} ms]')


start_time = time.time()
big_puzzle = parse_input(puzzle)

passports = []                                  # empty passport list from strings
for i in range(len(big_puzzle)):                # constructing passports
    passports.append(Passports(big_puzzle[i]))

print(f'Solution 2: {solution2(passports)} [Runtime: {round(((time.time() - start_time)*1000), 3)} ms]')
