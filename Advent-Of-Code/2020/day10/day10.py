def solution1(jolts):
    one, three = 0, 0           # count of differences of 'one' and 'three' jolts

    for i in range(len(jolts) - 1):
        if jolts[i + 1] - jolts[i] == 1:    # find and count differences of one jolt
            one += 1
        elif jolts[i + 1] - jolts[i] == 3:  # find and count differences of three jolts
            three += 1

    return one * three                      # return part 1 solution


with open("input.txt") as f:
    data = [int(line) for line in f]

data.append(0)              # start should be 0 jolts
data.append(max(data)+3)    # end is always 3 jolts higher than our last adapter
data.sort()

print(solution1(data))

checked = {}
def solution2(pos):

    if pos == len(data) - 1:
        return 1

    if pos in checked:
        return checked[pos]

    total = 0
    for i in range(pos + 1, len(data)):
        if data[i] - data[pos] <= 3:
            total += solution2(i)

    checked[pos] = total
    return total

print(solution2(0))