import time


def solution1(data):
    seats = []
    for j in data:
        row_low, row_high = 0, 127
        col_low, col_high = 0, 7
        seat_id_length = 0
        for i in j:                                             # parse trough string
            if i == 'F':
                seat_id_length += 1
                row_high -= round((row_high - row_low) / 2)     # if lower half, decrease highest row num
            if i == 'B':
                seat_id_length += 1
                row_low += round((row_high - row_low) / 2)      # if upper half, increase lower row num

            if i == 'R':
                col_low += round((col_high - col_low) / 2)      # if upper half, increase lower col num
            if i == 'L':
                col_high -= round((col_high - col_low) / 2)     # if lower half, decrease higher col num

        if j[seat_id_length - 1] == 'B':                # check if the last instruction for rows is upper or lower
            if j[-1] == 'R':                            # check upper/lower col
                seats.append(row_high * 8 + col_high)   # generate seat number
            else:
                seats.append(row_high * 8 + col_low)    # generate seat number
        elif j[seat_id_length - 1] == 'F':              # check if the last instruction for rows is upper or lower
            if j[-1] == 'R':                            # check upper/lower col
                seats.append(row_low * 8 + col_high)    # generate seat number
            else:
                seats.append(row_low * 8 + col_low)     # generate seat number
    return seats


def solution2(data):
    return [x for x in range(data[0], data[-1] + 1)     # parse trough seats to find a missing seat id
            if x not in data]                             # and return it


# quite slick solution found online
def not_my_solution(input_data):
    seat_ids = [int(seat_id.translate(str.maketrans("FBLR", "0101")), 2) for seat_id in input_data]
    valid_seat = [(len(seat_ids) + 1) // 2 * (min(seat_ids) + max(seat_ids)) - sum(seat_ids)]
    return max(seat_ids), valid_seat


start_time = time.time()
with open("input.txt") as f:
    puzzle = [line.rstrip() for line in f]


print(f'Solution 1: {max(solution1(puzzle))} [Runtime: {round(((time.time() - start_time)*1000), 3)} ms]')
start_time = time.time()
data_sorted = sorted(solution1(puzzle))
print(f'Solution 2: {solution2(data_sorted)} [Runtime: {round(((time.time() - start_time)*1000), 3)} ms]')

start_time = time.time()
print(f'Alternative solution: {not_my_solution(puzzle)} [Runtime: {round(((time.time() - start_time)*1000), 3)} ms]')
