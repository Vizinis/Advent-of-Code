def subset(set, n, sum):
    # Base Cases
    if sum == 0:
        return True
    if n == 0 and sum != 0:
        return False

    if set[n - 1] > sum:
        return subset(set, n - 1, sum);
    return subset(set, n - 1, sum) or subset(set, n - 1, sum - set[n - 1])


def solution1(puzzle):
    idx = 25    # how many numbers behind should we check
    index = 0   # current number
    for i in puzzle:
        set = [j for j in puzzle[index:index + idx]]
        n = len(set)
        if index + idx < len(puzzle):
            sum = puzzle[index + idx]
            if not subset(set, n, sum):
                return sum
        index += 1


def solution2(puzzle, part1):
    for i in range(len(puzzle) - 1):            # loop trough the whole puzzle, apart from the given number
        nums = [puzzle[i]]                      # start a new list, with the current number in the list

        for j in range(i + 1, len(puzzle)):     # loop trough the numbers from the number 'i' and add all of
            nums.append(puzzle[j])              # them to the nums list.

            if sum(nums) == part1:              # if nums list adds up to the given number
                return min(nums) + max(nums)    # return solution

            elif sum(nums) > part1:             # if nums list is bigger than given number, stop the loop
                break                           # reset nums list with a new number, and loop from there


with open("input.txt") as data:
    puzzle = [int(line) for line in data]


part1 = (solution1(puzzle))
print(part1)
print(solution2(puzzle, part1))
