def solution1(data):
    direction = "E"
    north_south, east_west = 0, 0
    for i in data:
        command, value = i[0], int(i[1:])
        if command == 'R':
            direction = rotate(direction, 'R', value)
        elif command == 'L':
            direction = rotate(direction, 'L', value)

        if command == 'F':
            if direction == 'N':
                north_south += value
            elif direction == 'S':
                north_south -= value
            elif direction == 'E':
                east_west += value
            elif direction == 'W':
                east_west -= value

        elif command == 'N':
            north_south += value
        elif command == 'S':
            north_south -= value
        elif command == 'E':
            east_west += value
        elif command == 'W':
            east_west -= value

    print(abs(north_south) + abs(east_west))


def rotate(current, side, degrees):
    dirs = ["N", "E", "S", "W"]             # compass
    index = dirs.index(current)             # find current index on compass
    if side == 'L':
        for i in range(int(degrees / 90)):  # rotate left given amount of times
            index -= 1
            if index < 0:                   # if rotated past north
                index = 3                   # assign west
        return dirs[index]
    if side == 'R':
        for i in range(int(degrees / 90)):  # rotate right
            index += 1
            if index == len(dirs):          # if at west
                index = 0                   # rotate to north
        return dirs[index]


def solution2(data):
    ship = [0, 0]   # ships x/y axis values. [0] - x, [1] - y
    wp = [10, 1]    # waypoints x/y values

    for line in data:
        command, value = line[0], int(line[1:])
        # rotate to the right
        if command == 'R':
            while value > 0:
                temp1 = wp[1]
                temp2 = wp[0] * -1
                wp[0], wp[1] = temp1, temp2
                value -= 90
        # rotate to the left
        elif command == 'L':
            while value > 0:
                temp1 = wp[1] * -1
                temp2 = wp[0]
                wp[0], wp[1] = temp1, temp2
                value -= 90
        # forward
        elif command == 'F':
            ship[0] += value * wp[0]
            ship[1] += value * wp[1]
        # calculate Y and X axis values
        elif command == 'N':
            wp[1] += value
        elif command == 'S':
            wp[1] -= value
        elif command == 'E':
            wp[0] += value
        elif command == 'W':
            wp[0] -= value
    print(abs(ship[0]) + abs(ship[1]))


with open("input.txt") as f:
    data = [line.rstrip() for line in f]

solution1(data)
solution2(data)
