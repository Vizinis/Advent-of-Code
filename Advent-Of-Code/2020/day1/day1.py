import itertools


# compare each element in the list with each other once
# if two elements give a sum of 2020
# multiply those elements together to get a solution.
def solution1(puzzle):
    for i in range(len(puzzle)):
        for j in range(i + 1, len(puzzle)):
            print(puzzle[i], puzzle[j])
#            if (puzzle[i] + puzzle[j]) == 2020:
#                print(puzzle[i] * puzzle[j])


# compare each element in the list with each other once
# if three elements give a sum of 2020
# multiply those elements together to get a solution.
def solution2(puzzle):
    for i in range(len(puzzle)):
        for j in range(i + 1, len(puzzle)):
            for k in range(j + 1, len(puzzle)):
                if (puzzle[i] + puzzle[j] + puzzle[k]) == 2020:
                    print(puzzle[i] * puzzle[j] * puzzle[k])


# alternative solution which does the same thing
# as solution1 and solution2 except this time
# using itertools.
def alternative(puzzle):
    # solution 1
    for a, b in itertools.combinations(puzzle, 2):
        if (a + b) == 2020:
            print(a * b)
    # solution 2
    for a, b, c in itertools.combinations(puzzle, 3):
        if (a + b + c) == 2020:
            print(a * b * c)


puzzle = []                                 # empty puzzle input
f = open("input.txt", "r")                  # open the input file
for i in f.readlines():                     # read the lines in the file. For every line remove "\n",
    puzzle.append(int(format(i.strip())))   # convert to int and add to the list
f.close()                                   # close the file

solution1(puzzle)
solution2(puzzle)
