from functools import reduce


def solution1(data):
    t = int(data[0])
    busses = {}
    wait_time = [0, 0] # wait time, bus id
    for i in data[1].split(','):
        if i != 'x':
            busses[int(i)] = 0

    for line in busses:
        while busses[line] < t:
            busses[line] += line

    for line in busses:
        if busses[line] - t < wait_time[0] or wait_time[0] == 0:
            wait_time[0] = busses[line] - t
            wait_time[1] = line

    print(wait_time[0] * wait_time[1])


with open("input.txt") as f:
    data = [line for line in f]

solution1(data)
#solution2(puzzle[1].split(','))


# part2 by this guy https://github.com/dylan-codesYT/AdventOfCode2020/blob/master/day13.py
data[1] = data[1].split(',')
def mod_inverse(a, n):
    # find some x such that (a*x) % n == 1
    a = a % n
    if n == 1:
        return 1
    for x in range(1, n):
        if (a * x) % n == 1:
            return x


# n busses
# bus k at index i departs at a time t+i
# t+i % k == 0
# t % k == -i
# t % k = k-i
# index = (k - (i%k)) % k
def get_earliest_time():
    ids = []
    fullProduct = 1
    for i in range(len(data[1])):
        item = data[1][i]
        if item != 'x':
            k = int(item)
            i = i % k
            ids.append(((k - i) % k, k))
            fullProduct *= k

    total = 0
    for i, k in ids:
        partialProduct = fullProduct // k

        inverse = mod_inverse(partialProduct, k)
        assert (inverse * partialProduct) % k == 1

        term = inverse * partialProduct * i
        total += term

    return total % fullProduct


print(get_earliest_time())