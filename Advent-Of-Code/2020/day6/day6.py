# remove empty lines, group tests
def parse_input(data):
    grouped_tests = []
    temp = []
    for i in data:
        if i not in "":
            temp.append(i)
        else:
            grouped_tests.append(temp)
            temp = []
    grouped_tests.append(temp)
    return grouped_tests


def solution1(data):
    total = 0
    for group in data:
        question_list = []  # reset list for new group
        for person in group:
            for question in person:
                question_list.append(question)              # add every answered question to a list
        total += len(set(question_list))                    # count only unique questions answered in the group
    print(total)


def solution2(data):
    total = 0               # count of all yes answers.
    for group in data:      # parse every group
        yes_questions = []  # questions answered yes within a group

        for person in group:            # gather all the answers from every person within a group
            for question in person:     # into yes_questions list
                yes_questions.append(question)

        count = 0                                       # count of questions to which everyone answered yes on
        for q in set(yes_questions):                    # traverse check every unique question answered within
            if yes_questions.count(q) == len(group):    # the group and check if everyone answered it
                count += 1

        total += count
    print(total)


with open("input.txt") as f:
    puzzle = [line.rstrip() for line in f]

puzzle = parse_input(puzzle)
solution1(puzzle)
solution2(puzzle)
