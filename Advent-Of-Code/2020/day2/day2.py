import re   # RegEx, https://regex101.com/


def count_letters(least, most, char, password):
    if least <= password.count(char) <= most:   # check if the char count fits the
        return True                             # criteria, return true if it does


def find_position(pos1, pos2, char, password):
    if password[pos1-1] == char and password[pos2-1] != char:       # check if the letter is in the first pos
        return True                                                 # if yes return true
    elif password[pos1-1] != char and password[pos2-1] == char:     # check if the letter is in the second pos
        return True                                                 # if yes return true


# get input from file in lines, strip \n
with open("input.txt") as f:
    content = [line.rstrip() for line in f]

# test = ["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"]

solution1 = 0
solution2 = 0
# parse every line, extract 2 numbers, a letter and a password using RegEx
# assign min, max, given character and a password variables
# send variables to count_letters for solution1
# send variables find_position for solution2
for i in content:
    for j in (re.findall('^(\d+)-(\d+) (\w): (\w+)', i)):
        least, most, char, password = int(j[0]), int(j[1]), j[2], j[3]
        if count_letters(least, most, char, password):
            solution1 += 1

        if find_position(least, most, char, password):
            solution2 += 1

print(solution1)
print(solution2)
