# Programming memes
1. [Advent of Code in Python for learnings.](https://gitlab.com/Vizinis/Advent-of-Code/-/tree/master/Advent-Of-Code)
    1. [2020](https://gitlab.com/Vizinis/Advent-of-Code/-/tree/master/Advent-Of-Code/2020)
    2. [2015](https://gitlab.com/Vizinis/Advent-of-Code/-/tree/master/Advent-Of-Code/2015)
2. [Flashcards generator](https://gitlab.com/Vizinis/Advent-of-Code/-/blob/master/flashcards.py) - Generates random flash cards from a given excel sheet.

****

# Azure/Microsoft certification compendiums

1. [AZ-900 Azure Fundamentals](https://gitlab.com/Vizinis/Advent-of-Code/-/blob/master/AZ900/az900.md)
2. [SC-900 Microsoft Security, Compliance, and Identity Fundamentals](https://gitlab.com/Vizinis/Advent-of-Code/-/blob/master/SC900/sc900.md)
